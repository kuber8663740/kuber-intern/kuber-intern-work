// const mysql = require('mysql2');
import mysql from 'mysql2';

// const dotenv = require('dotenv');
import dotenv from 'dotenv';
dotenv.config();

// const pool = mysql.createPool({
//   host: process.env.MYSQL_HOST,
//   user: process.env.MYSQL_USER,
//   password: process.env.MYSQL_PASSWORD,
//   database: process.env.MYSQL_DATABASE
// }).promise();

const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'kuber'
}).promise();


export async function getUsers() {
  try{
    const [rows] = await pool.query(`SELECT * FROM users`);  
    return rows;
  } catch (error) {
    console.error('Error fetching users: ', error);
    throw error;
  } 
}

export async function getUser(id){
  try{
    const [rows] = await pool.query(`SELECT * FROM users WHERE id = ?`, [id])  
    return rows;
  } catch(error) {
    console.error('Error fetching user: ', error);
    throw error;
  }
}

export async function createUser(email, password){
  try{
    const [result] = await pool.query(`
    INSERT INTO users (email, password)
    VALUES (?, ?)
    `, [email, password])  
    const id = result.insertId;
    return getUser(id);
  } catch (error) {
    console.error('Error creating user: ', error);
    throw error;
  }
}

export async function loginUser(email, password){
  try{
    const [existingUser] = await pool.query(
      'SELECT * FROM users WHERE email = ?', 
      [email]
    );

    if(!existingUser.length){
      return { error: 'User not found' };
    }
    else{
      if(existingUser[0].password !== password){
        return { error: 'Incorrect password' };
      }
      else{
        return { user: existingUser[0] };
      }
    }
  }catch(error){
    return{ error: 'An error occured' }
  }
}
